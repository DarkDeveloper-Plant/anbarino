/* $(window).on('scroll', function () {
    $(".box").each(function () {
        if (isScrolledIntoView($(this))) {
            $("#box-1").addClass("box-1");
            $("#box-2").addClass("box-2");
            $("#box-3").addClass("box-3");

        }
    });
});

function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom)&& (elemTop >= docViewTop));
} */


$('nav a, #showcase a, #footer .copyright .other .up')
.on('click',function (e) {

    if(this.hash !== ''){
      e.preventDefault();
      const hash = this.hash;
  
      $('html, body').animate({
        scrollTop:$(hash).offset().top 
      }, 600);
      
    }
  });

 

function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('#btn-bottom').offset().top - $(".container").offset().top;
    if (window_top > div_top /* && $(this).width() > 578 */) {
        $("#box-1").addClass("box-1");
        $("#box-2").addClass("box-2");
        $("#box-3").addClass("box-3");
    }
}

$(function () {
    $(window).scroll(sticky_relocate);
    sticky_relocate();
});



/* $(function(){

    $(window).bind("resize",function(){
        console.log('screen size:' + $(this).width())
        if($(this).width() < 578){
            $("#s").removeClass("fa-2x").addClass("fa-lg");
         
        }
      
    })
    });  */