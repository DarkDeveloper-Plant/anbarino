package ir.darkdeveloper.anbarino;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnbarinoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AnbarinoApplication.class, args);
    }

}
