package ir.darkdeveloper.anbarino.User;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import ir.darkdeveloper.anbarino.Product.ProductModel;
import ir.darkdeveloper.anbarino.User.Authority.OPAuthority;
import ir.darkdeveloper.anbarino.User.Authority.UserRoles;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Table(name = "user_tbl")
public class UserModel implements Serializable, UserDetails {

    @Id
    @GeneratedValue
    private Long id;

    private Boolean enabled = true;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles_tbl")
    private List<UserRoles> userRoles;

    @Column(unique = true)
    private String username;

    @Column(unique = true, nullable = false)
    private String email;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(nullable = false)
    private String password;

    private String shopName;

    private String shopImage;

    @Transient
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private MultipartFile file;

    private String address;

    private String description;

    //mappedBy is read-only. can't add product while creating user
    @OneToMany(mappedBy = "user")
    private List<ProductModel> products;

    @Column(name = "created_at", updatable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    private LocalDateTime updatedAt;



    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        if (userRoles != null && !userRoles.isEmpty()) {
            for (UserRoles role : userRoles) {
                grantedAuthorities.addAll(role.getAuthorities());
            }
        } else {
            //Default authority for a user
            grantedAuthorities.add(OPAuthority.OP_LOGIN);
            grantedAuthorities.add(OPAuthority.OP_DELETE_USER);
            grantedAuthorities.add(OPAuthority.OP_SIGIN_IN);
            grantedAuthorities.add(OPAuthority.OP_EDIT_PRODUCT);
            grantedAuthorities.add(OPAuthority.OP_ADD_PRODUCT);
            grantedAuthorities.add(OPAuthority.OP_DELETE_PRODUCT);
            grantedAuthorities.add(OPAuthority.OP_EDIT_USER);
        }
        return grantedAuthorities;
    }
}
