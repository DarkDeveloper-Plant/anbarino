package ir.darkdeveloper.anbarino.User;

import ir.darkdeveloper.anbarino.Config.Jwt.JwtConfig;
import ir.darkdeveloper.anbarino.Product.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Slf4j
public class UserService implements UserDetailsService {


    private final UserRepository repo;

    @Autowired
    public UserService(UserRepository repo) {
        this.repo = repo;
    }


    /**
     * check conditions in client
     **/
    @Transactional
    public ResponseEntity<?> saveUser(UserModel user, PasswordEncoder passwordEncoder, HttpServletResponse response
            , JwtConfig jwtConfig, AuthenticationManager authManager) {
        //Save user
        try {
            validateEmail(user.getEmail());
            String fileName = ProductService.saveFile(user.getFile());
            if (fileName != null) {
                user.setShopImage(fileName);
            }
            String password = passwordEncoder.encode(user.getPassword());
            String passwordBefore = user.getPassword();
            user.setPassword(password);
            repo.save(user);
            getUserToken(user, passwordBefore, response, jwtConfig, authManager);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);

    }


    public ResponseEntity<?> login(UserModel user, HttpServletResponse response
            , AuthenticationManager authManager, JwtConfig jwtConfig) {
        try {
            validateEmail(user.getEmail());
            getUserToken(user, user.getPassword(), response, jwtConfig, authManager);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }


    private void getUserToken(UserModel user, String passwordBefore, HttpServletResponse response
            , JwtConfig jwtConfig, AuthenticationManager authManager) {
        try {
            authManager.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), passwordBefore));
        } catch (Exception e) {
            e.printStackTrace();
        }
        response.addHeader("Authorization", jwtConfig.generateToken(user.getEmail()));
    }


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return repo.findByEmail(s);
    }

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private void validateEmail(String emailStr) throws Exception{
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        if (!matcher.find()){
            throw new Exception("Invalid email");
        }
    }

    public UserModel findUserById(Long id) {
        return repo.findUserModelById(id);
    }

    public ResponseEntity<?> deleteUser(UserModel user) {
        try {
            repo.delete(user);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Something went wrong while deleting user ", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("User deleted successfully", HttpStatus.OK);

    }

    public Page<UserModel> getAllUsers(Pageable pageable) {
        return repo.findAll(pageable);
    }

    public ResponseEntity<?> updateUser(UserModel user) {
        try {
            String fileName = ProductService.saveFile(user.getFile());
            if (fileName != null) {
                user.setShopImage(fileName);
            }
            if (user.getPassword() == null){
                String password = repo.findByEmail(user.getEmail()).getPassword();
                user.setPassword(password);
            }
            repo.save(user);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
