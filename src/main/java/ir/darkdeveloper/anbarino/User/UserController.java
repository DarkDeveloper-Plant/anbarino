package ir.darkdeveloper.anbarino.User;

import ir.darkdeveloper.anbarino.Config.Jwt.JwtConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private final JwtConfig jwtConfig;
    private final AuthenticationManager authManager;
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserController(JwtConfig jwtConfig, AuthenticationManager authManager
            , UserService rolesService, PasswordEncoder passwordEncoder) {
        this.jwtConfig = jwtConfig;
        this.authManager = authManager;
        this.userService = rolesService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping({"/signin", "/signin/"})
    public ResponseEntity<?> saveUser(@ModelAttribute UserModel user, HttpServletResponse response) {
        return userService.saveUser(user, passwordEncoder, response, jwtConfig, authManager);
    }

    @PostMapping({"/login/", "/login"})
    public ResponseEntity<?> loginUser(@ModelAttribute UserModel user, HttpServletResponse response) {
        return userService.login(user, response, authManager, jwtConfig);
    }


    //Only user himself and DarkDeveloper can delete user
    @PostMapping({"/delete", "/delete/"})
    @PreAuthorize("#user.email.equals(authentication.name) || #user.email.equals('DarkDeveloper')")
    public ResponseEntity<?> deleteUser(@RequestBody UserModel user) {
        return userService.deleteUser(user);
    }


    @PostMapping({"/update", "/update/"})
    @PreAuthorize("#user.email.equals(authentication.name) || #user.email.equals('DarkDeveloper')")
    public ResponseEntity<?> updateUser(@ModelAttribute UserModel user) {
        return userService.updateUser(user);
    }


    @GetMapping({"/{id}", "/{id}/"})
    public UserModel getUserInfo(@PathVariable Long id) {
        return userService.findUserById(id);
    }

    @GetMapping({"/all", "/all/"})
    @PreAuthorize("authentication.name.equals('DarkDeveloper')")
    public Page<UserModel> getUsers(Pageable pageable) {
        return userService.getAllUsers(pageable);
    }
}
