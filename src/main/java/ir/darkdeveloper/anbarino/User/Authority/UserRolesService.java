package ir.darkdeveloper.anbarino.User.Authority;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRolesService {
    private final UserRolesRepository repo;

    @Autowired
    public UserRolesService(UserRolesRepository repo) {
        this.repo = repo;
    }

    public UserRoles saveRole(UserRoles role){
        return repo.save(role);
    }

    public Boolean deleteRole(Long id){
        try {
            repo.deleteById(id);
            return true;

        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
