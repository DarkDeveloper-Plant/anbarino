package ir.darkdeveloper.anbarino.User.Authority;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
public class UserRolesController {

    private final UserRolesService service;

    @Autowired
    public UserRolesController(UserRolesService service) {
        this.service = service;
    }

    @PostMapping({"/role/add", "/role/add/"})
    @PreAuthorize("hasAuthority('OP_ADD_ROLE')")
    public UserRoles saveRole(@RequestBody UserRoles role){
        return service.saveRole(role);
    }

    @PostMapping({"/role/add/{id}", "/role/add/{id}/"})
    @PreAuthorize("hasAuthority('OP_DELETE_ROLE')")
    public Boolean deleteRole(@PathVariable("id") Long id){
        return service.deleteRole(id);
    }
}
