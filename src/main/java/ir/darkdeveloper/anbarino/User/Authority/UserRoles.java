package ir.darkdeveloper.anbarino.User.Authority;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "roles_tbl")
public class UserRoles {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    //referencing to an other column name rather than primary key(id): should implement Serializable
    @ElementCollection(targetClass = OPAuthority.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "roles_auth_tbl")
    private List<OPAuthority> authorities;

}
