package ir.darkdeveloper.anbarino.User.Authority;

import org.springframework.security.core.GrantedAuthority;

public enum OPAuthority implements GrantedAuthority {
    OP_SIGIN_IN,
    OP_LOGIN,
    OP_EDIT_PRODUCT,
    OP_ADD_PRODUCT,
    OP_DELETE_PRODUCT,
    OP_DELETE_USER,
    OP_EDIT_USER,

    OP_ADD_ROLE,
    OP_DELETE_ROLE;

    @Override
    public String getAuthority() {
        return this.name();
    }
}
