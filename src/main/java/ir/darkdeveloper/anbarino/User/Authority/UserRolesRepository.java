package ir.darkdeveloper.anbarino.User.Authority;

import org.hibernate.cfg.JPAIndexHolder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.swing.*;

@Repository
public interface UserRolesRepository extends JpaRepository<UserRoles, Long> {

}
