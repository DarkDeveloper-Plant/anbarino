package ir.darkdeveloper.anbarino.User;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<UserModel, Long> {

    @Query("SELECT users from UserModel users where users.email = :email ")
    UserModel findByEmail(@Param("email") String email);

    UserModel findUserModelById(Long id);

    Page<UserModel> findAll(Pageable pageable);
}
