package ir.darkdeveloper.anbarino.Product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<ProductModel, Long> {

    Page<ProductModel> findByNameContains(String name, Pageable pageable);
}
