package ir.darkdeveloper.anbarino.Product;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import ir.darkdeveloper.anbarino.User.UserModel;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Proxy;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Table(name = "product_tbl")
public class ProductModel {

    @Id
    @GeneratedValue
    private Long id;

    @Column(length = 50, nullable = false)
    private String name;

    private String description;

    private String image;

    @Transient
    private MultipartFile file;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false, name = "sell_price")
    private Double sellPrice;

    @Column(nullable = false, name = "buy_price")
    private Double buyPrice;

    @Column(nullable = false, name = "sell_count")
    private int soldCount;

    @Column(nullable = false, name = "buy_count")
    private int boughtCount;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserModel user;


    @Column(nullable = false, name = "total_count")
    private int totalCount;

    @Column(name = "created_at", updatable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    private LocalDateTime updatedAt;


}
